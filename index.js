var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

//var path = require('path'); //Failed to lookup view "home" in views directory "/views"
app.set('view engine','ejs');
app.set('views','./views'); // . is important

app.use(express.static(__dirname+'/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(cookieParser());

app.use(session({
    secret:'session_secret_key',
    resave:true,
    saveUninitialized:false
}));

var userinput={
    "username":"admin",
    "password":"admin"
}

var students = [
    {"id":1,"name":"Ranjith","studentId":"0127889023","dept":"SE"},
    {"id":2,"name":"Alex","studentId":"0137287388","dept":"CE"},
    {"id":3,"name":"Bob","studentId":"1827389200","dept":"CSE"}
];

app.get('/',function(req,res){
    if(req.session.user){
        res.render('userInfo');
    }else{
        res.render('home');
    }
});

app.post('/home',function(req,res){
    
    console.log("Inside Home Login Page");

    if(req.session.user){
        res.render('userInfo');
    }else{
        if( userinput.username === req.body.uname && userinput.password ===req.body.pass){
            req.session.user = userinput;
            res.render('userInfo');
        }else{
            console.log("Invalid login credentials");
        }
    }
});

app.post('/userInfo',(req,res)=>{

    console.log("Inside user information input page");
    console.log("USER SESSION" , req.session.user);

    if(!req.session.user){
        res.redirect('/');
    }else{
        var i=4;
        var newStudent = {"id":i++,"name":req.body.name,"studentId":req.body.studentId,"dept":req.body.dept};
        students.push(newStudent);
        console.log("New Student added");
        console.log("Total list now is ",students);
        res.render('userReport',{students:students});
    }
});

app.post('/userReport',(req,res)=>{
    console.log("Inside user Report display page");
    
    //traverse through all the student objects, get all the IDs
    var ids = students.map(function(student){
        return student.id;
    });
    //console.log(ids);
    //console.log(req.body.delete); //student id to be deleted.
    //console.log(ids.indexOf(parseInt(req.body.delete)));

    //find the index of deleted item in ids, splice removes that index value and updates students
    //parseInt must be used as req.body.delete is string and ids are Int so -1 will be return by indexOf
    students.splice(ids.indexOf(parseInt(req.body.delete)),1); 
    console.log("after deleting student",students);
    res.render('userReport',{students:students});
});

app.listen(3000, ()=>{console.log("Server started at local host 3000 port")});





